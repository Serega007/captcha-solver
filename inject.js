if (window.location.href.match(/.hcaptcha.com\/captcha.v\d\//)) {
    let widgetsList = document.createElement('yandex-captcha-solver')
    document.head.appendChild(widgetsList)
	
    let script = document.createElement('script')
    script.innerHTML = `
        if (document.getElementById('checkbox') == null) {
        	if (typeof hcaptcha !== 'undefined') {
        		hcaptcha.core.Language.setLocale('ru')
        	} else {
                const timer0 = setInterval(()=> {
                    if (typeof hcaptcha !== 'undefined') {
                        hcaptcha.core.Language.setLocale('ru')
                        clearInterval(timer0)
                    }
                }, 100)
        	}
        }
    `
//     script.src = chrome.runtime.getURL('lang.js')
    document.documentElement.appendChild(script)
//     script.onload = function() {
//         script.remove()
//     }
    
//     const timer1 = setInterval(()=>{
//         if (document.getElementById('checkbox') != null
//             && isScrolledIntoView(document.getElementById('checkbox'))
//             && document.getElementById('checkbox').style.display !== 'none') {
//             document.getElementById('checkbox').click()
//             clearInterval(timer1)
//         }
//     }, 1000)
    
    let timer2 = setInterval(async function funcTimer2() {
        if (document.querySelector('.challenge-container') == null) {
            clearInterval(timer2)
            return
        }
        if (document.querySelector('.challenge-container').childElementCount > 0 && document.querySelector('div.image-wrapper').firstElementChild.style.backgroundImage !== '' && document.querySelectorAll('.task-image[aria-pressed="true"]').length === 0) {
            for (const el of document.querySelectorAll('div.image-wrapper')) {
                if (el.firstElementChild.style.backgroundImage === '') return
            }
            clearInterval(timer2)

			const div = document.createElement('div')
			div.style.textAlign = 'right'
			div.style.color = 'rgb(0 8 255)'
			div.textContent = 'Идёт автоматическое решение капчи...'
			document.querySelector('.challenge-interface').lastChild.after(div)

            const question = findSynonyms(document.querySelector('.prompt-text').innerHTML, {hcaptcha: true})
            
            const promises = []
            let passCount = 0
            let reseiveCount = 0
            const elements = document.querySelectorAll('div.image-wrapper')
            for (const el of elements) {
            	const url = el.firstElementChild.style.backgroundImage.replace('url("', '').replace('")', '')
            	const response = await fetch(url)
            	const blob = await response.blob()
            	const dataURL = await blobToDataURL(blob)
//             	const canvas = document.createElement('canvas')
//             	const ctx = canvas.getContext('2d')
//             	ctx.drawImage(el.firstElementChild.computedStyleMap().get('background-image'), 0, 0)
//             	const blob = await new Promise(resolve => canvas.toBlob(resolve, 'image/png'))

//              const url = el.firstElementChild.style.backgroundImage.replace('url("', '').replace('")', '')
                const promise = new Promise((resolve, reject) => {
                    chrome.runtime.sendMessage({dataURL, question, hcaptcha: true}, function(response) {
                    	reseiveCount++
                    	if (!response || response.error) {
                    		if (elements.length === reseiveCount) {
                    			if (passCount < 3) {
                    	            div.style.color = '#da5e5e'
                    	            div.textContent = 'Ошибка, response error'
                    			    reject()
                    			    return
                    			}
                    		}
                    		resolve()
                    		return
                    	} else if (response.captcha) {
                    		if (elements.length === reseiveCount) {
                    			if (passCount < 3) {
                    	            div.style.color = '#da5e5e'
                    	            div.textContent = 'Ошибка, Yandex просит пройти капчу'
                    	            reject()
                    	            return
                    			}
                    		}
                    		resolve()
                    		return
                    	}
                        console.log(el, response[0].url, response[0].answer)
                        if (response[0].answer) {
                        	el.click()
                        	passCount++
                        }
                        resolve()
                    })
                })
                promises.push(promise)
            }
            await Promise.all(promises)
            await wait(1000)
			div.remove()
// 			timer2 = setInterval(funcTimer2, 1000)
// 			return
            document.querySelector('.button-submit.button').click()
            timer2 = setInterval(funcTimer2, 1000)
        }
    }, 1000)
}/* else if (window.location.href.match(/https:\/\/www.google.com\/recaptcha\/api\d\/bframe/) || window.location.href.match(/https:\/\/www.recaptcha.net\/recaptcha\/api\d\/bframe/)) {
    let widgetsList = document.createElement('yandex-captcha-solver')
    document.head.appendChild(widgetsList)
	
	const lang = document.URL.match(/hl=(.*?)&/).pop()
	if (lang !== 'ru') {//Да нам нужен русский, да это неудобно для инностранцев, не нравится - не пользуйтесь тогда этим расширением
		document.location.replace(document.URL.replace(/hl=(.*?)&/, 'hl=ru&'))
	}

	let timer3 = setInterval(async function funcTimer3() {
		if (document.getElementById('solver-button') == null && document.getElementById('rc-imageselect') != null && isScrolledIntoView(document.getElementById('rc-imageselect')) && document.querySelector('.rc-imageselect-payload') != null && document.querySelector('.rc-imageselect-payload').lastChild != null) {
			clearInterval(timer3)

			const div = document.createElement('div')
			div.style.textAlign = 'right'
			div.style.color = 'rgb(0 8 255)'
			div.textContent = 'Идёт автоматическое решение капчи...'
			document.querySelector('.rc-imageselect-payload').lastChild.after(div)
            
			const instruction = document.querySelector('.rc-imageselect-instructions strong').previousSibling.textContent
			if (document.querySelector('.rc-imageselect-error-select-more').style.display != 'none' || document.querySelector('.rc-imageselect-error-dynamic-more').style.display != 'none') {
				await wait(500)
				const rand = Math.floor(Math.random() * 8)
				document.querySelectorAll('.rc-image-tile-wrapper img')[rand].click()
				if (document.querySelector('.rc-imageselect-instructions span') != null) {//Если тут изображения которые пропадают и новое появляются
					await imageExclusion(rand, findSynonyms(document.querySelector('.rc-imageselect-instructions strong').textContent, {recaptcha: true}))
				}
// 				document.getElementById('recaptcha-verify-button').click()
// 				await wait(500)
			} else if (instruction === 'Выберите все изображения, где есть ' || instruction === 'Выберите все изображения с ') {
                const question = findSynonyms(document.querySelector('.rc-imageselect-instructions strong').textContent, {recaptcha: true})
		    	
                const img = document.querySelector('.rc-image-tile-wrapper img')
                const response = await fetch(img.src)
            	const blob = await response.blob()
            	const dataURL = await blobToDataURL(blob)
                await new Promise((resolve, reject) => {
                    chrome.runtime.sendMessage({dataURL, question, recaptcha: true, multipleImages: true}, async function(response) {
                    	if (!response || response.error) {
                    		reject()
                    		return
                    	}
                    	let count = 0
                    	const promises = []
                        for (const el of document.querySelectorAll('.rc-image-tile-wrapper img')) {
                        	console.log(el.parentElement, response[count].url, response[count].answer)
                        	if (response[count].answer) {
                        		el.click()
                                if (document.querySelector('.rc-imageselect-instructions span') != null) {//Если тут изображения которые пропадают и новое появляются
                                	promises.push(imageExclusion(count, question))
                                }
                        	}
                            count++
                        }
                        await Promise.all(promises)
                        resolve()
                    })
                })
                
		    }

			document.querySelector('.rc-imageselect-incorrect-response').style.display = 'none'
			document.querySelector('.rc-imageselect-incorrect-response').removeAttribute('tabindex')
			document.querySelector('.rc-imageselect-error-select-more').style.display = 'none'
			document.querySelector('.rc-imageselect-error-select-more').removeAttribute('tabindex')
			document.querySelector('.rc-imageselect-error-dynamic-more').style.display = 'none'
			document.querySelector('.rc-imageselect-error-dynamic-more').removeAttribute('tabindex')

			document.getElementById('recaptcha-verify-button').click()
			await wait(1000)

// 			return

		    div.remove()
		    timer3 = setInterval(funcTimer3, 1000)
		}
    }, 1000)

	async function imageExclusion(count, question) {
		let loop = true
		while(loop) {
			await wait(6000)
			loop = await disappearImage(document.querySelectorAll('.rc-image-tile-wrapper img')[count], question)
		}
	}

    async function disappearImage(img, question) {
		const response = await fetch(img.src)
		const blob = await response.blob()
		const dataURL = await blobToDataURL(blob)
		return new Promise((resolve, reject) => {
			chrome.runtime.sendMessage({dataURL, question, recaptcha: true}, function(response) {
				if (!response && response.error) {
					reject()
					return
				}
				console.log(img.parentElement, response[0].url, response[0].answer)
				if (response[0].answer) img.click()
				resolve(response[0].answer)
			})
		})
    }
}*/

function findSynonyms(text, captcha) {
	text = text.replaceAll('&nbsp;', ' ').replace('Выберите все фото с ', '').replace('ами', '').replace('Щелкните каждое изображение, на котором изображен ', '').replace('Пожалуйста, нажмите на каждое изображение, содержащее ', '').replace('Пожалуйста, щелкните каждое изображение с ', '').replace('Щелкните каждое изображение с ', '')
	console.log('Сработало')
	if (text === 'автомобили') text = 'машин'
	if (text === 'лодкой') text = 'лодки'
	if (text === 'автобусом') text = 'автобус'
	const question = [text]
	if (text === 'машин') {
		question.push('автомобил')
// 		if (captcha.recaptcha) question.push('дорог')
	} else if (text === 'грузовик') {
		question.push('тягач')
		question.push('грузовой автомобиль')
		question.push('gruzovik')
		question.push('камаз')
//      question.push('truck')
	} else if (text === 'мотоцикл') {
		question.push('motorcycle')
	} else if (text === 'самолет') {
		question.push('самолёт')
		question.push('авиация')
		question.push('airplane')
//      question.push('аэродром')
	} else if (text === 'пешеходные переходы') {
		question.push('пешеходный переход')
	} else if (text === 'мост') {
		question.push('путепровод')
	} else if (text === 'велосипеды') {
		question.push('велосипед')
	} else if (text === 'лодки' || text === 'лодк') {
		question.push('лодка')
		question.push('катер')
	} else if (text === 'трактора') {
		question.push('трактор')
	}
	return question
}

function isScrolledIntoView(el) {
    const rect = el.getBoundingClientRect()
    const elemTop = rect.top
    const elemBottom = rect.bottom

    // Only completely visible elements return true:
    let isVisible// = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    isVisible = elemTop < window.innerHeight && elemBottom >= 0
    return isVisible
}

async function wait(ms) {
    return new Promise(resolve=>{
        setTimeout(()=>{
            resolve()
        }, ms)
    })
}

async function blobToDataURL(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onloadend = ()=>resolve(reader.result)
        reader.onerror = reject
        reader.readAsDataURL(blob)
    })
}