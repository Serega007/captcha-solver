chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    getAnswer(request, sendResponse)
    return true
})

async function getAnswer(request, sendResponse) {
    try {
        console.log(request)
        //Метод blob
        const blob = dataURLtoBlob(request.dataURL)
        const formData = new FormData()
        formData.append('upfile', blob)
        let response = await fetch('https://yandex.ru/images/search?rpt=imageview&format=json&request=%7B%22blocks%22%3A%5B%7B%22block%22%3A%22cbir-controller__upload%3Aajax%22%7D%5D%7D', {
          'method': 'POST',
          'Sec-Fetch-Mode': 'same-origin',
          'Sec-Fetch-Site': 'none',
          'Sec-Fetch-Dest': 'empty',
          'body': formData
        })
        const json = await response.json()
        if (json.captcha) {
            sendResponse({captcha: true})
            console.warn('captcha require')
            return
        }
        
        const firstURL = request.multipleImages ? 'https://yandex.ru/images/search?rpt=imageview&url=' + encodeURIComponent(json.blocks[0].params.originalImageUrl) : 'https://yandex.ru/images/search?' + json.blocks[0].params.url
        const answers = []
        for (let i = 0; i < (request.multipleImages ? 9 : 1); i++) {
            let url
            if (request.multipleImages) {
                if (i === 0) url = firstURL + '&crop=0%3B0%3B0.3333%3B0.3333'
                else if (i === 1) url = firstURL + '&crop=0.3333%3B0%3B0.6666%3B0.3333'
                else if (i === 2) url = firstURL + '&crop=0.6666%3B0%3B1%3B0.3333'
                else if (i === 3) url = firstURL + '&crop=0%3B0.3333%3B0.3333%3B0.6666'
                else if (i === 4) url = firstURL + '&crop=0.3333%3B0.3333%3B0.6666%3B0.6666'
                else if (i === 5) url = firstURL + '&crop=0.6666%3B0.3333%3B1%3B0.6666'
                else if (i === 6) url = firstURL + '&crop=0%3B0.6666%3B0.3333%3B1'
                else if (i === 7) url = firstURL + '&crop=0.3333%3B0.6666%3B0.6666%3B1'
                else if (i === 8) url = firstURL + '&crop=0.6666%3B0.6666%3B1%3B1'
            } else {
                url = firstURL
            }
            response = await fetch(url, {
              'method': 'GET',
              'Sec-Fetch-Mode': 'same-origin',
              'Sec-Fetch-Site': 'none',
              'Sec-Fetch-Dest': 'empty',
            })
            response.html = await response.text()
            response.doc = new DOMParser().parseFromString(response.html, 'text/html')

            if (response.doc.querySelector('.CheckboxCaptcha') != null) {
                sendResponse({captcha: true})
                console.warn('captcha require')
                return
            }
            
            if (response.doc.querySelector('.misspell__message') != null) {
//                 console.warn(response.doc.querySelector('.misspell__message').textContent)
                answers.push({answer: false, url: response.doc.querySelector('.misspell__message').textContent})
                continue
            }
            if (response.doc.querySelector('.CbirItem.CbirSites.CbirSites_infinite') != null) response.doc.querySelector('.CbirItem.CbirSites.CbirSites_infinite').parentElement.remove()
            if (response.doc.querySelector('.CbirItem.CbirSites.CbirSites_simple') != null) response.doc.querySelector('.CbirItem.CbirSites.CbirSites_simple').parentElement.remove()
            const html = response.doc.body.outerHTML.toLowerCase()
            let _continue = false
            for (const question of request.question) {
                if (html.includes(question)) {
                    answers.push({answer: true, url: url})
                    _continue = true
                    break
                }
            }
            if (_continue) continue
            answers.push({answer: false, url: url})
        }

        sendResponse(answers)
    } catch (e) {
        sendResponse({error: true})
        console.error(e)
    }

}

function dataURLtoBlob(dataURL) {
    const byteString = atob(dataURL.split(',')[1]),
        mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0]

    const ab = new ArrayBuffer(byteString.length)
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i)
    }

    const blob = new Blob([ia], {type: mimeString})
    return blob
}